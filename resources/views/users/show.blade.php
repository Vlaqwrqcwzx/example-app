<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP Test</title>
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
</head>
<body>
<div class="wrapper">
    <main class="main-content">
        <div class="my-profile">
            <h2 class="heading">Мой профиль</h2>
            <div class="profile">
                <div class="avatar">
                    @if($user->avatar)
                        <img src="{{ \App\Components\FileManager::getPathByFile($user->avatar) }}" alt="Аватар" class="avatar__pic">
                    @else
                        <img src="{{ asset('images/image.jpg') }}" alt="Аватар" class="avatar__pic">
                    @endif
                </div>
                <div class="information">
                    <div class="nickname">{{ $user->nickname }}</div>
                    <div class="user-name">
                        <span class="name">{{ $user->name }}</span>
                        <span class="surname">{{ $user->surname }}</span>
                    </div>
                    <a href='tel:{{$user->phone}}' class="phone">{{ $user->phone }}</a>
                </div>
            </div>
        </div>
        <div class='edit-profile'>
            <h2 class="heading">Профиль</h2>
            <form class='form' id='form' method='POST' enctype='multipart/form-data'>
                <ul class="form__list">
                    <li class="form__item">
                        <label class='form__label' for="nickname">Никнейм:</label>
                        <input class='form__input' id='nickname' type="text" value="{{ $user->nickname }}" readonly>
                    </li>
                    <li class="form__item">
                        <label class='form__label' for="name">Имя:</label>
                        <input class='form__input' id='name' type="text" value="{{ $user->name }}" readonly>
                    </li>
                    <li class="form__item">
                        <label class='form__label' for="surname">Фамилия:</label>
                        <input class='form__input' id='surname' type="text" value="{{ $user->surname }}" readonly>
                    </li>
                    <li class="form__item">
                        <label class='form__label' for="phone">Телефон:</label>
                        <input class='form__input' id='phone' type="text" value="{{ $user->phone }}" readonly>
                    </li>
                    <li class="form__item">
                        <label class='form__label' for="email">Почта:</label>
                        <input class='form__input' id='email' type="email" value="{{ $user->email }}" readonly>
                    </li>
                    <li class="form__item">
                        <label class="form__title">Пол:</label>
                        <input class='form__input' id='gender' type="text" value="{{ \App\Models\User::$genders[$user->gender] }}" readonly>
                    </li>
                    <li class="form__item">
                        @if($user->email_notify)
                            <label class='form__inline-label' for="showPhone">Я согласен получать email-рассылку</label>
                        @endif
                    </li>
                </ul>
            </form>
        </div>
    </main>
</div>
</body>
</html>
