<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\{
    Database\Eloquent\Relations\HasMany,
    Notifications\Notifiable,
    Database\Eloquent\Factories\HasFactory,
    Foundation\Auth\User as Authenticatable
};

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    public const MALE = 0;
    public const FEMALE = 1;

    public static array $genders = [
        self::MALE  => 'male',
        self::FEMALE => 'female',
    ];

    protected $fillable = [
        'name',
        'surname',
        'nickname',
        'avatar',
        'gender',
        'email_notify',
        'phone',
        'email',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'email_notify'      => 'boolean',
    ];

    public function articles(): HasMany
    {
        return $this->hasMany(Article::class);
    }
}
