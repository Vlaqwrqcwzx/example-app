<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\{
    Model,
    Relations\BelongsTo,
    SoftDeletes
};

class Article extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'text',
        'deleted_at'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function isAuthor(User $user): bool|null
    {
        if ($this->trashed()) {
            return null;
        }

        return $this->user->id === $user->id;
    }
}
