<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\{Http\Requests\StoreUserRequest, Models\Article, Services\UserService, Models\User};
use Illuminate\View\View;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    private UserService $userService;

    private string $templatePath = 'users.';

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function create(): View
    {
        return view($this->templatePath . __FUNCTION__);
    }

    public function store(StoreUserRequest $request)
    {
        $user = $this->userService->storeElement($request);

        return redirect()->route('users.show', $user);
    }

    public function show(User $user): View
    {
        return view($this->templatePath . __FUNCTION__, compact('user'));
    }
}
