<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\{
    Foundation\Http\FormRequest
};

class StoreUserRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name'          => ['nullable', 'string', 'max:255'],
            'surname'       => ['nullable', 'string', 'max:255'],
            'nickname'      => ['nullable', 'string', 'max:255'],
            'avatar'        => ['nullable', 'max:20000', 'image'],
            'gender'        => ['nullable', 'boolean'],
            'email_notify'  => ['nullable', 'boolean'],
            'phone'         => ['nullable', 'string', 'max:255', 'unique:users'],
            'email'         => ['required', 'string', 'max:255', 'email', 'unique:users'],
        ];
    }

    public function messages(): array
    {
        return [
            'name.string'           => __('users.validation.name.string'),
            'name.max'              => __('users.validation.name.max'),
            'surname.string'        => __('users.validation.surname.string'),
            'surname.max'           => __('users.validation.surname.max'),
            'nickname.string'       => __('users.validation.nickname.string'),
            'nickname.max'          => __('users.validation.nickname.max'),
            'avatar.max'            => __('events.validation.avatar.max'),
            'avatar.image'          => __('events.validation.avatar.image'),
            'gender.boolean'        => __('events.validation.gender.boolean'),
            'email_notify.boolean'  => __('events.validation.email_notify.boolean'),
            'phone.string'          => __('users.validation.phone.string'),
            'phone.max'             => __('users.validation.phone.max'),
            'phone.unique'          => __('users.validation.phone.unique'),
            'email.string'          => __('users.validation.email.string'),
            'email.email'           => __('users.validation.email.email'),
            'email.max'             => __('users.validation.email.max'),
            'email.unique'          => __('users.validation.email.unique'),
        ];
    }
}
