<?php

declare(strict_types=1);

namespace App\Components;

use Illuminate\{
    Http\UploadedFile,
    Support\Facades\Storage,
    Support\Str
};

class FileManager
{
    public const DISPERSION = 2;
    public const DEPTH = 2;

    public static function getName(): string
    {
        return time() . '_' . Str::random(10);
    }

    public static function getNameWithoutExtension(string $fileName): string
    {
        return pathinfo($fileName,PATHINFO_FILENAME);
    }

    public static function createPath(string $fileName): string
    {
        $beginningLine = substr($fileName, 0, self::DISPERSION);
        $endLine = substr($fileName, -self::DISPERSION);

        return $beginningLine . DIRECTORY_SEPARATOR . $endLine;
    }

    public static function storeFile(UploadedFile $file, string $fileName = null): string
    {
        $fileName = $fileName ?? self::getName();
        $fileNameMimeType = $fileName . '.' . $file->getClientOriginalExtension();
        $path = self::createPath(self::getNameWithoutExtension($fileName));
        Storage::disk('public')->putFileAs($path, $file, $fileNameMimeType);

        return $fileNameMimeType;
    }

    public static function deleteFile(string $fileName): bool
    {
        $path = self::createPath(self::getNameWithoutExtension($fileName));

        if (!Storage::disk('public')->exists($path . DIRECTORY_SEPARATOR . $fileName)) {
            return false;
        }

        return Storage::disk('public')->delete($path . DIRECTORY_SEPARATOR . $fileName);
    }

    public static function getPathByFile(string $fileName): string
    {
        $path = self::createPath(self::getNameWithoutExtension($fileName));

        return Storage::disk('public')->url($path . DIRECTORY_SEPARATOR . $fileName);
    }
}
