<?php

declare(strict_types=1);

namespace App\Services;

use App\Components\FileManager;
use App\Models\User;
use Illuminate\Http\Request;

class UserService
{
    public function storeElement(Request $request): User
    {
        $user = User::create($request->all());

        if ($request->avatar) {
            $user->update([
                'avatar' => FileManager::storeFile($request->avatar),
            ]);
        }

        return $user;
    }
}
