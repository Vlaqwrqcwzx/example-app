<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::get('/', [UserController::class, 'create']);
Route::resource('users', UserController::class)->only(['show', 'store']);
